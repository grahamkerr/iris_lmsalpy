# -*- coding: utf-8 -*-
"""
Created on Tue Sep  5 13:51:03 2017

@author: asainz.solarphysics@gmail.com

This routine fits an IRIS Mg II h&K Level 2 profile
with 4, 5, or 7 (default) Gaussian profiles.
It works  when the spectral range of the IRIS profile
is 2794.0028 < wl_ref_vac [AA] < 2806.0199 or 
   2793.1791 < wl_ref_air [AA] < 2805.1933, a spectral
sampling of 0.02545 AA, and normalized with respect to
3.454e6

"""

import numpy as np
import matplotlib.pyplot as plt
from IPython.core import debugger ; debug = debugger.Pdb().set_trace
from scipy.optimize import curve_fit


def calculate(iris_prof, iris_wl, show_plot = True, four = False, five = False, 
              verbose = False, in_vac = True, cont_level = 3.454e6, 
              interpolate = False):

    """ Fits an IRIS Mg II h&k profile with 2793.1791 < wl_ref[AA] < 2805.1933 and 
    a spectral sampling of 0.02545 AA with 4, 5, or 7 (defualt) Gaussian profiles.
    It returns the total profile, the individual Gaussian profiles, and the
    offset constant in Y and all the parameters for the Gaussian profiles 

    Parameters
    ----------

    iris_prof: np.array, IRIS Mg II h&k level 2 profile evaluated at wl_ref

    show_plot: boolean, if True, the code shows the original IRIS profile and 
    the sum of the Gaussian profiles. Default = True

    four: boolean, if True, the code tries to fit the IRIS profile with 4
    Gaussian profiles. Default = False

    five: boolean, if True, the code tries to fit the IRIS profile with 5
    Gaussian profiles. Default = False

    verbose: boolean, it True the code prints some values of the fit. 
    Default =  False

    Returns
    -------

    fit_iris: np.array, the sum of all the Gaussian profiles used to fit the
    IRIS profile

    list_gaussians: list of np.array, the individual Gaussian profiles used to
    fit the IRIS profile

    best_vals: np.array, with the constan offset in Y (c), and the amplitud, 
    mu and sigma of each of the Gauassian profile. Its size can be 22 (1+3*7),
    13 (1+3*4), or 16 (1+3*5)

    Example
    -------

    aux = sv.load('data_iris2peaks.jbl.gz')
    prof_iris = aux['prof_iris']
    wl_iris = aux['wl_iris']
    del aux

    # A nice easy profile
    fit_iris_10, list_gauss_10, best_val_10 = iris2peaks.calculate(prof_iris[10,:], in_vac = True, cont_level = 1.)
    # A single-peaked profile
    fit_iris_16, list_gauss_16, best_val_16 = iris2peaks.calculate(prof_iris[16,:], in_vac = True, cont_level = 1.)
    # A challenging one, still doing a quite decent job
    fit_iris_14, list_gauss_14, best_val_14 = iris2peaks.calculate(prof_iris[14,:], in_vac = True, cont_level = 1.)


    WARNING: For IRIS data in DNs just run:
        fit_iris, list_gauss, best_val = iris2peaks.calculate(prof_iris_in_DNs, wl_iris, cont_level = 1.)
        or 
        fit_iris, list_gauss, best_val = iris2peaks.calculate(iris_raster.raster['Mg II k 2796'].data[100,100,:], iris_raster.raster['Mg II k 2796'].wl)
    """

    if in_vac == True: 
        wl_0 = np.argmin(np.abs(iris_wl - 2794.0028))
        wl_1 = np.argmin(np.abs(iris_wl - 2806.0199))
        iris_prof = iris_prof[wl_0:wl_1+1].copy()
        wl_ref = np.linspace(2794.002766643432, 2806.0198862746115, 473)
        if interpolate == True: iris_prof = np.interp(wl_ref, iris_wl[wl_0:wl_1+1], iris_prof)
    else:
        print()
        print('WARNING: Considering wl values in the air')
        print()
        wl_0 = np.argmin(np.abs(iris_wl - 2793.1791))
        wl_1 = np.argmin(np.abs(iris_wl - 2805.1933))
        iris_prof = iris_prof[wl_0:wl_1+1].copy()
        wl_ref = np.linspace(2793.17909767, 2805.19329454, 473)
        if interpolate == True: iris_prof = np.interp(wl_ref, iris_wl[wl_0:wl_1+1], iris_prof)

    init_vals = (0.,0.01,0.,5.,0.01,88.,5.,0.01,97.,5.,0.01,250.,50.,0.01,370.,5.,0.01,378.,5.,0.01,450.,5)

    limits  = ([0.,0.,-5.,5.,0.,80.,1.,0.,92.,1.,0.,200.,20.,0.,360.,1.,0.,374.,1.,0.,400.,1],
                [5.,5.,5.,50.,20.,92.,49.,20.,105.,49.,20.,300.,1000.,20.,374.,49.,20.,385.,49.,5.,470.,50.])


    if five == True: 
       init_vals = (0.,0.01,88.,5.,0.01,97.,5.,0.01,250.,50.,0.01,370.,5.,0.01,378.,5.)
       limits  = ([0.,0.,80.,1.,0.,92.,1.,0.,200.,20.,0.,360.,1.,0.,374.,1.,],
                   [5.,20.,92.,30.,20.,105.,30,20.,300.,1000.,20.,374.,30.,20.,385.,30.])
    if four== True: 
       init_vals = (0.,0.01,88.,5.,0.01,97.,5.,0.01,370.,5.,0.01,378.,5.)
       limits  = ([0.,0.,60.,1.,0.,92,1.,0.,350.,1.,0.,374.,1.,],
                   [5.,20.,92.,30.,20.,120,30.,20.,374.,30.,20.,400,30.])

    dim =  iris_prof.shape
    x = np.arange(dim[0], dtype=np.float64)

    if verbose == True:
        print(iris_prof.shape)
        print(len(init_vals))
        print(len(limits[0]))
        print(len(limits[1]))

    best_vals, covar = curve_fit(sum_gaussian, x, iris_prof/cont_level, p0=init_vals,
                                 bounds = limits) 
    if verbose == True:
        print(init_vals)
        print(best_vals )

    fit_iris = sum_gaussian(x, *best_vals)
    fit_iris = fit_iris*cont_level

    list_gaussians = []
    if show_plot == True:
       plt.figure(figsize=(12,5))
       plt.plot(wl_ref, iris_prof)
       plt.plot(wl_ref, fit_iris)
       for j, i in enumerate(range(0,best_vals[1:].shape[0],3)):
           list_gaussians.append(sum_gaussian(x,best_vals[0],
                                              *best_vals[i+1:i+4])*cont_level)
           plt.plot(wl_ref, list_gaussians[j], ls = ':')
       plt.show()

    return fit_iris, list_gaussians, best_vals


def sum_gaussian(x, *param_gauss):

    """ Calculates a sum of N Gaussians with parameters c, amp1, mu1, sigma1,
    ... ampN, muN, sigmaN, with c as offset constant in Y """

    param_gauss_ok = np.array(param_gauss)[1:]
    param_gauss_ok = param_gauss_ok.reshape(int(param_gauss_ok.size/3),3)
    g = param_gauss[0]
    count = 0
    for i in param_gauss_ok:
        g = g +  i[0] * np.exp(-(1.0/2.0)*((x-i[1])/i[2])**2)
    return g


### Just here for educational purposes 
# def sum_gaussian_ok(x, c, amp1, mu1, sigma1, amp2, mu2, sigma2,
#                   amp3, mu3, sigma3, amp4, mu4, sigma4,
#                   amp5, mu5, sigma5, amp6, mu6, sigma6,
#                   amp7, mu7, sigma7):
#
#    param_ok = np.array([amp1, mu1, sigma1, amp2, mu2, sigma2,
#                        amp3, mu3, sigma3, amp4, mu4, sigma4,
#                        amp5, mu5, sigma5, amp6, mu6, sigma6,
#                        amp7, mu7, sigma7])
#    param_ok = param_ok.reshape(int(param_ok.size/3),3)
#    g = 0
#    for i in param_ok:
#        # print  i[0], i[1], i[2]
#        g = g + i[0] * np.exp(-(1.0/2.0)*((x-i[1])/i[2])**2)
#    return g + c 
