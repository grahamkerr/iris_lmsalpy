### Authors: Magnus M. Woods, built from functions sum_gaussian and calculate written by Alberto Sainz Dalda
### 19/06/2020: Original date
### 22/06/2020: Added v_nt calculator
### 24/06/2020: Added error values for fit paramters as an output.
import numpy as np
import matplotlib.pyplot as plt
from IPython.core import debugger ; debug = debugger.Pdb().set_trace
from scipy.optimize import curve_fit
import scipy.constants as const
from scipy.interpolate import make_interp_spline, BSpline

def fit_gen(iris_prof, iris_wl ,init_vals, limits, wl_range, show_plot=True, lorentz=False, verbose=False):
    """ Fits an IRIS profile with N user defined
    Gaussian profiles or N user defined lorentzian profiles. 
    
    It returns the total fitted profile, the individual Gaussian/lorentzian profiles, and the
    offset constant in Y and all the parameters for the Gaussian/lorentzian profiles. 
    
    Parameters
    ----------

    iris_prof: np.array, IRIS Si IV 1403 level 2 profile 
    
    iris_wl: np.array, The wavelength array corresponding to the giving iris_prof
    
    init_vals: list, with parameters c, amp1, mu1, sigma1,... ampN, muN, sigmaN,
                     with c as offset constant in Y. 
                     For a lorentzian distribution these parameters are c, amp1, x0_1, gamma1,
                     ... ampN, x0_N, gammaN, with:
    
                     c     =  offset constant in Y. 
                     amp   = amplitude of the profile
                     x0    = location parameter
                     gamma = half-width at half maximum

    limits: 2xN list, upper and lower limits coresponging to init_vals. 
    
    wl_range: list, min and max wavelength over which fitting is carried out. 

    show_plot: boolean, if True, the code shows the original IRIS profile and 
               the sum of the Gaussian profiles. Default = True
    
    lorentz: boolean, if True, will attempt to fit a single lorentzian profile at the Si IV rest wvl.
             Default = False
    
    verbose: boolean, it True the code prints some values of the fit. Default =  False
    
    Returns
    -------

    iris_fit: np.array, the sum of all the profiles used to fit the
    IRIS profile

    list_gaussians: list of np.array, the individual profiles used to
    fit the IRIS profile

    best_vals: np.array, with the constan offset in Y (c), and the amplitude, 
    mu and sigma of each of the Gauassian profile or  amplitude, x0 and gamma

    errors: np.array, one stanmdard deviation errors for all parameters in best_vals

    Example
    -------
    For any given iris line:

    iris_spec = iris_raster.raster['desired_iris_line'].data[100,5,:]
    iris_wl = iris_raster.raster['desired_iris_line'].wl

    iris_fit, list_gauss, best_val, errors = iris_lmsalpy.fit_iris.fit_gen(iris_spec, iris_wl)
    """

    wl_0 = np.argmin(np.abs(iris_wl - wl_range[0]))
    wl_1 = np.argmin(np.abs(iris_wl - wl_range[1]))
    iris_prof = iris_prof[wl_0:wl_1+1].copy()
    wl_ref = iris_wl[wl_0:wl_1+1].copy()
        
    x = wl_ref

    if len(limits[0]) != len(limits[1]):
        print('Warning: Upper and lower limits must have the same len')
        print('Lower limit len = '+str(len(limits[0])))
        print('Upper limit len = '+str(len(limits[1])))
        return None
    else:
        if len(init_vals) != len(limits[0]):
            print('Warning: Initial Values and Upper and lower limits must have the same len')
            print('Initial value len = '+str(len(init_vals)))
            print('Limit len = '+str(len(limits[0])))
            return None


    if not lorentz:
        fit_func = sum_gaussian
    else:
        fit_func  = sum_lorentz
    
    if verbose:
        print('The inital fit values are:')
        print(init_vals)
        print()
        print('Limits are:')
        print(limits[0])
        print(limits[1])
        print()
    
    best_vals, covar = curve_fit(fit_func, x, iris_prof, p0=init_vals,
                                 bounds = limits) 
    
    errors = np.sqrt(np.diag(covar))
    
    if verbose:
        print('The inital fit values are:')
        print(init_vals)
        print()
        print('The final fit values are:')
        print(best_vals )
        print()

    iris_fit = fit_func(x, *best_vals)
    iris_fit = iris_fit

    list_gaussians = []

    if show_plot is True:
        plt.figure(figsize=(12,5))
        plt.plot(wl_ref, iris_prof)
        plt.plot(wl_ref, iris_fit)

        for j, i in enumerate(range(0,best_vals[1:].shape[0],3)):
            list_gaussians.append(fit_func(x,best_vals[0], *best_vals[i+1:i+4]))
            plt.plot(wl_ref, list_gaussians[j], ls = ':')
        plt.show()

    # Pad iris_fit and list_gaussians to the original length of iris_prof
    strt_pd = np.empty(wl_0)
    strt_pd[:] = np.nan
    end_pd = np.empty(len(iris_wl)-(wl_1+1))
    end_pd[:] = np.nan

    iris_fit = np.concatenate((strt_pd, iris_fit, end_pd))
    
    for i, e in enumerate(list_gaussians):
        list_gaussians[i] = np.concatenate((strt_pd, e, end_pd))

    return iris_fit, list_gaussians, best_vals, errors

def fit_siiv(iris_prof, iris_wl, show_plot=True, two=False, three=False, lorentz=False,
              init_vals=None, limits=None, wl_range = None, verbose=False ):

    """ Fits an IRIS Si IV 1403 profile with 1 (Default), 3 or N, user defined
    Gaussian profiles. It can also fit one (Default) or N, user defined lorentzian profiles. 
    
    It returns the total fitted profile, the individual Gaussian/lorentzian profiles, and the
    offset constant in Y and all the parameters for the Gaussian/lorentzian profiles. 
    
    Parameters
    ----------

    iris_prof: np.array, IRIS Si IV 1403 level 2 profile 
    
    iris_wl: np.array, The wavelength array corresponding to the giving iris_prof

    show_plot: boolean, if True, the code shows the original IRIS profile and 
               the sum of the Gaussian profiles. Default = True
    
    two: boolean, if True, the code tries to fit the IRIS profile with 2
                   Gaussian profiles. Default = False

    three: boolean, if True, the code tries to fit the IRIS profile with 3
                   Gaussian profiles. Default = False
    
    lorentz: boolean, if True, will attempt to fit 1 (default) or N user defined lorentzian profile at the Si IV rest wvl.
                    Default = False
    
    init_vals: list, with parameters c, amp1, mu1, sigma1,... ampN, muN, sigmaN,
                    with c as offset constant in Y. 
                    For a lorentzian distribution these parameters are c, amp1, x0_1, gamma1,
                    ... ampN, x0_N, gammaN, with:
    
                    c     =  offset constant in Y. 
                    amp   = amplitude of the profile
                    x0    = location parameter
                    gamma = half-width at half maximum
                    
                    Default = None 
    
    limits: 2xN list, upper and lower limits coresponging to init_vals. Defult = None
    
    wl_range: list, min and max wavelength over which fitting is carried out. Default = None
    
    verbose: boolean, it True the code prints some values of the fit. Default =  False
    
    Returns
    -------

    iris_fit: np.array, the sum of all the profiles used to fit the
    IRIS profile

    list_gaussians: list of np.array, the individual profiles used to
    fit the IRIS profile

    best_vals: np.array, with the constan offset in Y (c), and the amplitude, 
    mu and sigma of each of the Gauassian profile or  amplitude, x0 and gamma

    errors: np.array, one stanmdard deviation errors for all parameters in best_vals

    Example
    -------
    
    iris_spec = iris_raster.raster['Si IV 1403'].data[100,5,:]
    iris_wl = iris_raster.raster['Si IV 1403'].wl

    iris_fit, list_gauss, best_val = iris_lmsalpy.fit_iris.fit_siiv(iris_spec, iris_wl)

    """
    if wl_range is None:
        wl_range = [1402., 1403.5]

    
    wl_0 = np.argmin(np.abs(iris_wl - wl_range[0]))
    wl_1 = np.argmin(np.abs(iris_wl - wl_range[1]))
    iris_prof = iris_prof[wl_0:wl_1+1].copy()
    wl_ref = iris_wl[wl_0:wl_1+1].copy()
#     if interpolate is True: iris_prof = np.interp(wl_ref, iris_wl[wl_0:wl_1+1], iris_prof)
    
    
    if not all(v is None for v in [init_vals, limits]):
        if init_vals is None and limits is not None:
            print('Warning: Please provide coresponding init_vals for chosen limits')
            return None
        if init_vals is not None and limits is None:
            print('Warning: Please provide coresponding limits for chosen init_vals')
            return None

        if len(limits[0]) != len(limits[1]):
            print('Warning: Upper and lower limits must have the same len')
            print('Lower limit len = '+str(len(limits[0])))
            print('Upper limit len = '+str(len(limits[1])))
            return None
        else:
            if len(init_vals) != len(limits[0]):
                print('Warning: Initial Values and Upper and lower limits must have the same len')
                print('Initial value len = '+str(len(init_vals)))
                print('Limit len = '+str(len(limits[0])))
                return None
    else:
        if not lorentz:
            if three:
                if verbose:
                    print('Fitting three Gaussians')
                    print()
                init_vals = [0.,50,1402.6,0.1,200,1402.77,0.1,50,1402.85,0.1]
                limits = [[0.,0.,1402.52,0.01,0.,1402.69,0.01,0.,1402.73, 0.01 ],
                         [5., 5000.,1402.67,0.1,5000.,1402.85,0.1,5000.,1402.93,0.1]]
            elif two:
                if verbose:
                    print('Fitting two Gaussians')
                    print()

                init_vals = [0.,200,1402.70,0.1,50,1402.85,0.1]
                limits = [[0.,0.,1402.62,0.01,0.,1402.73, 0.01 ],
                         [5., 5000.,1402.77,0.1,5000.,1402.93,0.1]]
            else:
                if verbose:
                    print('Fitting one Gaussians')
                    print()
                init_vals = [0.,200,1402.77,0.1]
                limits = [[0.,0.,1402.69,0.01],
                        [5.,5000.,1402.85,0.1]]
        else:
            if verbose:
                    print('Fitting one lorentzian')
                    print()
            init_vals = [0.,200,1402.77,0.1]
            limits = [[0.,0.,1402.63,0.001],
                      [5.,5000.,1402.9,0.2]]
        
    x = wl_ref
    
    if not lorentz:
        fit_func = sum_gaussian
    else:
        fit_func  = sum_lorentz
    
    if verbose:
        print('The inital fit values are:')
        print(init_vals)
        print()
        print('Limits are:')
        print(limits[0])
        print(limits[1])
        print()
    
    best_vals, covar = curve_fit(fit_func, x, iris_prof, p0=init_vals,
                                 bounds = limits) 
    
    errors = np.sqrt(np.diag(covar)) 

    if verbose:
        print('The inital fit values are:')
        print(init_vals)
        print()
        print('The final fit values are:')
        print(best_vals )
        print()

    iris_fit = fit_func(x, *best_vals)
    iris_fit = iris_fit

    list_gaussians = []
    if show_plot is True:
        plt.figure(figsize=(12,5))
        plt.plot(wl_ref, iris_prof)
        plt.plot(wl_ref, iris_fit)

        for j, i in enumerate(range(0,best_vals[1:].shape[0],3)):
            list_gaussians.append(fit_func(x,best_vals[0],
                                              *best_vals[i+1:i+4]))
            plt.plot(wl_ref, list_gaussians[j], ls = ':')
        plt.show()

    # Pad iris_fit and list_gaussians to the original length of iris_prof
    strt_pd = np.empty(wl_0)
    strt_pd[:] = np.nan
    end_pd = np.empty(len(iris_wl)-(wl_1+1))
    end_pd[:] = np.nan

    iris_fit = np.concatenate((strt_pd, iris_fit, end_pd))
    
    for i, e in enumerate(list_gaussians):
        list_gaussians[i] = np.concatenate((strt_pd, e, end_pd))
    
    return iris_fit, list_gaussians, best_vals, errors
    

def fit_mgii(iris_prof, iris_wl, show_plot=True, four=False, five=False, 
              init_vals=None, limits=None, wl_range=None, verbose=False, in_vac=True, 
              cont_level=3.454e6, interpolate=False):

    """ Fits a IRIS Mg II h&k profile with 4, 5, 7 (defualt) or N user defined Gaussian profiles.
    It returns the total fitted profile, the individual Gaussian profiles, and the
    offset constant in Y and all the parameters for the Gaussian profiles. 

    Parameters
    ----------

    iris_prof: np.array, IRIS Mg II h&k level 2 profile 
    
    iris_wl: np.array, The wavelength array corresponding to the giving iris_prof

    show_plot: boolean, if True, the code shows the original IRIS profile and 
               the sum of the Gaussian profiles. Default = True

    four: boolean, if True, the code tries to fit the IRIS profile with 4
                   Gaussian profiles. Default = False

    five: boolean, if True, the code tries to fit the IRIS profile with 5
                   Gaussian profiles. Default = False
    
    init_vals: list, with parameters c, amp1, mu1, sigma1,
                    ... ampN, muN, sigmaN, with c as offset constant in Y. Default = None 
    
    limits: 2xN list, upper and lower limits coresponging to init_vals. Defult = None
    
    wl_range: list, min and max wavelength over which fitting is carried out. Default = None
    
    verbose: boolean, it True the code prints some values of the fit. Default =  False
    
    cont_level: float, Offset due to continuum. Default = 3.454e6
    
    interpolate: boolean,  Default = False

    Returns
    -------

    iris_fit: np.array, the sum of all the Gaussian profiles used to fit the
    IRIS profile

    list_gaussians: list of np.array, the individual Gaussian profiles used to
    fit the IRIS profile

    best_vals: np.array, with the constan offset in Y (c), and the amplitud, 
    mu and sigma of each of the Gauassian profile. Its size can be 22 (1+3*7),
    13 (1+3*4), or 16 (1+3*5)

    errors: np.array, one stanmdard deviation errors for all parameters in best_vals

    Example
    -------
    For a specrum in vacuum (this will be the most common case):

    iris_spec = iris_raster.raster['Mg II k 2796'].data[100,5,:]
    iris_wl = iris_raster.raster['Mg II k 2796'].wl

    iris_fit, list_gauss, best_val = iris_lmsalpy.fit_iris.fit_mgii(iris_spec, iris_wl)


    For a spectrum in air:

    iris_fit, list_gauss, best_val = iris_lmsalpy.fit_iris.fit_mgii(iris_spec, iris_wl in_vac = True, cont_level = 1.)



    
        
    """
    if wl_range is None:
        if in_vac:
            wl_range = [2794.0028, 2806.0199]
        else:
            print()
            print('WARNING: Considering wl values in the air')
            print()
            wl_range = [2793.1791, 2805.1933]
    
    wl_0 = np.argmin(np.abs(iris_wl - wl_range[0]))
    wl_1 = np.argmin(np.abs(iris_wl - wl_range[1]))
    iris_prof = iris_prof[wl_0:wl_1+1].copy()
    wl_ref = iris_wl[wl_0:wl_1+1].copy()
    if interpolate: iris_prof = np.interp(wl_ref, iris_wl[wl_0:wl_1+1], iris_prof)
    
    
    if not all(v is None for v in [init_vals, limits]):
        if init_vals is None and limits is not None:
            print('Warning: Please provide coresponding init_vals for chosen limits')
            return None
        if init_vals is not None and limits is None:
            print('Warning: Please provide coresponding limits for chosen init_vals')
            return None
    else:
        if not five and not four:
            print('Fitting seven Gaussians')
            print()
        init_vals = [0.,0.01,2794.0027,0.25,0.01,2796.2432,0.25,0.01,2796.4723,0.25,0.01,2800.3677,2.5,0.01,2803.4229,0.25,0.01,2803.6266,0.25,0.01,2805.4597,0.25]

        limits = [[0.,0.,2793.8754,0.25,0.,2796.0395,0.05,0.,2796.3450,0.05,0.,2799.0947,1.0,0.,2803.1683,0.05,0.,2803.5248,0.05,0.,2804.1867,0.05], 
                  [5.,5.,2794.1300,2.5,20.,2796.34503,2.45,20.,2796.6760,2.45,20.,2801.6407,50.,20.,2803.52480,2.45,20.,2803.8048,2.45,5.,2805.9689,2.5]]
        
        if five: 
            print('Fitting five Gaussians')
            print()

            init_vals = [0.,0.01,2796.2432,0.25,0.01,2796.4723,0.25,0.01,2800.3677,2.5,0.01,2803.4229,0.25,0.01,2803.6266,0.25]

            limits = [[0.,0.,2796.0395,0.05,0.,2796.34506,0.05,0.,2799.0947,1.0,0.,2803.1683,0.05,0.,2803.5248,0.05], 
                      [5.,20.,2796.3450,2.45,20.,2796.6760,2.45,20.,2801.6407,50.,20.,2803.5248,2.45,20.,2803.8048,2.45]]
        if four:
            print('Fitting four Gaussians')
            print()

            init_vals = [0.,0.,2796.0395,0.05,0.,2796.3450,0.05,0.,2803.1683,0.05,0.,2803.5248063511885,0.05]

            limits = [[0.,0.,2796.0395,0.05,0.,2796.3450,0.05,0.,2803.1683,0.05,0.,2803.5248,0.05], 
                      [5.,20.,2796.3450,2.45,20.,2796.6760,2.45,20.,2803.5248,2.45,20.,2803.8048,2.45]]
    
        if not in_vac:
            for i in range(2, len(init_vals), 3):
                init_vals[i] = init_vals[i] - 0.82515
                limits[0][i] = limits[0][i] - 0.82515
                limits[1][i] = limits[1][i] - 0.82515
        
    x = wl_ref
    
    if verbose:
        print('The inital fit values are:')
        print(init_vals)
        print()
        print('Limits are:')
        print(limits[0])
        print(limits[1])
        print()
    
    best_vals, covar = curve_fit(sum_gaussian, x, iris_prof/cont_level, p0=init_vals,
                                 bounds = limits) 
    
    errors = np.sqrt(np.diag(covar))

    if verbose:
        print('The inital fit values are:')
        print(init_vals)
        print()
        print('The final fit values are:')
        print(best_vals )
        print()

    iris_fit = sum_gaussian(x, *best_vals)
    iris_fit = iris_fit*cont_level

    list_gaussians = []
    if show_plot:
        plt.figure(figsize=(12,5))
        plt.plot(wl_ref, iris_prof)
        plt.plot(wl_ref, iris_fit)

        for j, i in enumerate(range(0,best_vals[1:].shape[0],3)):
            list_gaussians.append(sum_gaussian(x,best_vals[0],
                                              *best_vals[i+1:i+4])*cont_level)
            plt.plot(wl_ref, list_gaussians[j], ls = ':')
        plt.show()

    # Pad iris_fit and list_gaussians to the original length of iris_prof
    strt_pd = np.empty(wl_0)
    strt_pd[:] = np.nan
    end_pd = np.empty(len(iris_wl)-(wl_1+1))
    end_pd[:] = np.nan

    iris_fit = np.concatenate((strt_pd, iris_fit, end_pd))
    
    for i, e in enumerate(list_gaussians):
        list_gaussians[i] = np.concatenate((strt_pd, e, end_pd))
    
    return iris_fit, list_gaussians, best_vals, errors


def sum_gaussian(x,  *param_gauss):

    """ Calculates a sum of N Gaussians with parameters c, amp1, mu1, sigma1,
    ... ampN, muN, sigmaN, with c as offset constant in Y """

    params_gauss_ok = np.array(param_gauss)[1:]
    params_gauss_ok = params_gauss_ok.reshape(int(params_gauss_ok.size/3),3)
    g = param_gauss[0]
    count = 0
    for i in params_gauss_ok:
        g = g +  i[0] * np.exp(-(1.0/2.0)*((x-i[1])/i[2])**2)
    return g

def sum_lorentz(x, *param_lorentz):
    """ Calculates a sum of N lorentzians with parameters c, amp1, x0_1, gamma1,
    ... ampN, x0_N, gammaN, with:
    
    c     =  offset constant in Y. 
    amp   = amplitude of the profile
    x0    = location parameter
    gamma = half-width at half maximum
    """
    param_lorentz_ok = np.array(param_lorentz)[1:]
    param_lorentz_ok = param_lorentz_ok.reshape(int(param_lorentz_ok.size/3),3)
    f = param_lorentz[0]
    for i in param_lorentz_ok:
        f = f + i[0]*((i[2]**2)/(((x-i[1])**2) +i[2]**2))
    return f

def calc_fwhm(iris_fit, iris_wl):
    """
    This function calculates the Full Width Half Maximum for a given spectra. 
    
    Parameters
    ----------
    
    iris_fit: np.array, the sum of all the Gaussian profiles used to fit the
    IRIS profile
    
    iris_wl: np.array, The wavelength array corresponding to the given IRIS profile
    
    Returns
    -------
    
    fwhm: float, the FWHM in angstrom (Å)
    
    """
    max_y = np.nanmax(iris_fit)

    # pick out the spectrum (e.g. get rid of the nans)
    spec = iris_fit[~np.isnan(iris_fit)]
    wl = iris_wl[~np.isnan(iris_fit)]

    # Now interpolate
    wl_int = np.linspace(wl.min(), wl.max(), 300) 
    spl = make_interp_spline(wl, spec, k=3)  # type: BSpline
    spec_int = spl(wl_int)

    # Make a line at the half max
    f = np.full((len(spec_int)), max_y/2)

    # Find the FWHM
    arg = np.argwhere(np.diff(np.sign(f-spec_int)).flatten())

    fwhm_comp = [wl_int[arg[0][0]],wl_int[arg[1][0]]]

    fwhm = fwhm_comp[1]- fwhm_comp[0]
    
    return fwhm

def iris_vnt(element, ion, wvl, obs_wid, inst_wid=None, ti=None, nt_fwhm=False, therm_v=False, verbose=False):
    """
    Compute the thermal and non-thermal velocity of a given spectral line in km/s.
    
    Parameters
    ----------
    
    element: string, the element that is being studied. 
    
    ion: string, the ionization state of the line. 
    
    wvl: float, the nominal rest wavelength of the line measured in Angstrom (Å)
    
    obs_wid: float, the observed FWHM of the line, measured in Angstrom (Å)
    
    inst_wid: float, the instrumental FWHM in Angrstrom (Å). For IRIS the instrumental FWHM are:
                    FUV short = 0.0286Å
                    FUV long = 0.0318Å
                    
                    If an instrumental width is not supplied the value zero will be used. Default = None 
    
    ti: float, the peak ion temperature in the ionizations balance, measured in logT. If not supplied,
                the value from ions.txt will be used. Default = None
                
    nt_fwhm: boolean, if this is set the routine will return the non-thermal FWHM in Angstrom (Å) 
                    along with the non-thermal velocity. Default = False
                    
    therm_v: boolean,  if set the routine will return the thermal velocity of the line in km/s. Default = False
    
    verbose: boolean, it True the routine prints some values of the fit. Default = False
    
    Returns
    -------
    v_nt: float, the non-thermal velocity of the line, in km/s
    
    fwhm_nt: float, if therm_v is True, the non-thermal FWHM of the lime is returned, in Angstrom(Å)
    
    v_t: float, if therm_v is True, the thermal velocity is returned, in km/s
    
    
    Example
    -------
    
    v_nt = iris_vnt('Si', 'iv', 1402.77, obs_fwhm ,inst_wid=0.0286, verbose=True)
    
    
    Version History:
    ----------------
    original version: 17/06/2020, Author Magnus M. Woods, Translated and adapted from Dr Harry Warren's IDL 
                                                            routine eis_width2velocity.pro
    
    """
    
    element = element.upper()
    ion = ion.upper()
    cc = const.c/1000 # c in km/s
    
    # Read in the ions.txt file 
    #This is a duplicate of Dr. Harry Warren's file of temperature and thermal velocity, eis_width2velocity.dat, in SSW. 
    ion_dat = np.loadtxt('ions.txt', dtype='U')
    elements = np.asarray(list(map(str.upper, ion_dat[1:, 0])))
    ions = ion_dat[1:,1]
    t_max = ion_dat[1:,2].astype(float)
    v_therm = ion_dat[1:,3].astype(float)
    
    elms,ins = np.array([i for i, n in enumerate(elements) if n == element]),np.array([i for i, n in enumerate(ions) if n == ion])
    
    match = []
    for i in enumerate(elms):
        for j in enumerate(ins):
            if i[1] == j[1]: match.append(i[1])
    count = len(match)
    
    if count > 0:
        if ti is None:
            ti_max = t_max[match[0]]
            v_t = v_therm[match[0]]
            if verbose:
                print('Peak T in ion balance (ti_max) = '+str(ti_max))
                print()
                print('Thermal velocity = '+str(v_t))
                print()
        else:
            # Load info in about various at0mic masses
            masses_dat = np.loadtxt('element_masses.txt', dtype='U')
            elem_ms = np.asarray(list(map(str.upper, masses_dat[1:, 0])))
            mass_g = masses_dat[1:, 2].astype(float)
            # Get the mass of the ion we want to study
            mass = mass_g[np.where(elem == match[0])[0][0]]

            v_t = np.sqrt(2*(10.0**ti)*1.6022**-12/11604.0/mass_g)/1.0**5
            ti_max = ti
            if verbose:
                print('Peak T in ion balance (ti_max) = '+str(ti_max))
                print()
                print('Thermal velocity = '+str(ti_max))
                print()
            
        # Thermal FWHM in Å
        therm_fwhm = np.sqrt(4*np.log(2))*wvl*v_t/cc
        
        if inst_wid is None:
            inst_wid = 0
            print('Warning: Calculating V_nt without instrumental width')
            print()
        
        # Non-thermal FWHM in Å
        fwhm_nt_2 = (obs_wid**2 - inst_wid**2 - therm_fwhm**2)
        
        # non-thermal velocity
        if fwhm_nt_2 > 0:
            v_nt = cc*(np.sqrt(fwhm_nt_2)/wvl)/np.sqrt(4*np.log(2))
            fwhm_nt = np.sqrt(fwhm_nt_2)
        else:
            print('Warning: Observed width is smaller than thermal width')
            print()
            v_nt = -1
            fwhm_nt = -1
            
    if nt_fwhm and not therm_v:
        if verbose:
            print('Returning non-thermal velocity and non-thermal FWHM')
            print()
            print('Non-thermal velocity = '+str(v_nt)+'km/s')
            print('Non-thermal FWHM = '+str(fwhm_nt)+'Å')
            print()
        return v_nt, fwhm_nt
    
    elif therm_v and not nt_fwhm:
        if verbose:
            print('Returning non-thermal velocity and thermal velocity')
            print()
            print('Non-thermal velocity = '+str(v_nt)+'km/s')
            print('Thermal velocity = '+str(v_t)+'km/s')
            print()
        return v_nt, v_t
    
    elif nt_fwhm and therm_v:
        if verbose:
            print('Returning non-thermal velocity, non-thermal FWHM, and thermal velocity')
            print()
            print('Non-thermal velocity = '+str(v_nt)+'km/s')
            print('Non-thermal FWHM = '+str(fwhm_nt)+'Å')
            print('Thermal velocity = '+str(v_t)+'km/s')
            print()
        return v_nt,fwhm_nt, v_t
    else:
        if verbose:
            print('Returning non-thermal velocity')
            print()
            print('Non-thermal velocity = '+str(v_nt)+'km/s')
        return v_nt