import matplotlib.pylab as plt
import numpy as np
from astropy.io import fits
from astropy.io.fits import getheader
# import find
from IPython.core import debugger ; debug = debugger.Pdb().set_trace

# filename = '/Users/asainz/ciencia/IRIS/my_large/iris_l2_20170917_134745_3600108077_raster_t000_r00000.fits'
# donde = '/Users/asainz/ciencia/IRIS/my_large/'
# file   = 'iris_l2_20170917_134745_3600108077_raster_t000_r00000.fits'
#
# data = get_data_l2.get(filename, window_info=['Mg II k 2796'], verbose=True)
# lines = lines = get_data_l2.show_lines(filename)
# data = get_data_l2.get(filename, window_info=lines[1])
#  

def get(filename, nover=True, window_info = ['Mg II k 2796'], verbose = 0,
        showtime = False, showY = False, ext_opt =0, no_data = False):
  
    # self.name =  filename 
    hdulist = fits.open(filename)
    if verbose !=0:
       print()
       print('Reading file {}... '.format(filename))
       hdulist.info()
    hdr = hdulist[0].header
    nwin  = hdr['NWIN']
   
    out = {}

    if verbose !=0:
       print()
       print('Available data are stored in windows labeled as:')
       # print()
       for i in range(nwin): print(i+1, hdr['TDESC{}'.format(i+1)])

    for label_window_info in window_info:
        label_window_ok = label_window_info.replace(' ', '_')
        for i in range(nwin): 
            if hdr['TDESC{}'.format(i+1)].find(label_window_info) != -1: windx = i+1

        if verbose !=0:
           print()
           print('Reading data for requested window = {0} {1}'.format(windx,hdr['TDESC{}'.format(windx)]))

        data = hdulist[windx].data
        # hdulist.close()
        data = np.transpose(data, [1,0,2])

        if verbose !=0:
           print('Size of the read data from window = {0} {1}: {2}'.format(windx,hdr['TDESC{}'.format(windx)], data.shape))
           print()


        hdr_windx_0 = hdr.cards['*{}'.format(windx)]
        hdr_windx = getheader(filename, windx)
        # print(hdr_windx_0)
        # print(hdr_windx)

        DATE_OBS = hdr['DATE_OBS']
        DATE_END = hdr['DATE_END']
        TDET   = hdr['TDET{}'.format(windx)]
        TDESCT = hdr['TDESC{}'.format(windx)]
        TWAVE  = hdr['TWAVE{}'.format(windx)]
        TWMIN  = hdr['TWMIN{}'.format(windx)]
        TWMAX  = hdr['TWMAX{}'.format(windx)]
        SPCSCL = hdr_windx['CDELT1']
        SPXSCL = hdr_windx['CDELT3']
        SPYSCL = hdr_windx['CDELT2']
        POS_X  = hdr_windx['CRVAL3']
        POS_Y  = hdr_windx['CRVAL2']
        STEPT_AV = hdr['STEPT_AV']

        wl = (np.array(range(hdulist[windx].header['NAXIS1'])) * SPCSCL) + TWMIN

        # print(DATE_OBS, TDET, TDESCT, TWAVE, TWMIN, TWMAX, SPCSCL, SPXSCL, SPYSCL, POS_X, POS_Y)
     
        pos_ref = filename.find('iris_l2_')
        date_in_filename = filename[pos_ref+8:pos_ref+23]
        iris_obs_code    = filename[pos_ref+24:pos_ref+34]
        raster_info      = filename[pos_ref+35:pos_ref+53] 

        extent_arcsec_arcsec = [0, data.shape[1]*SPXSCL, 0, data.shape[0]*SPYSCL]
        extent_px_px = [0, data.shape[1]*1., 0, data.shape[0]*1.] 
        extent_px_arcsec  = [0, data.shape[1]*1., 0, data.shape[0]*SPYSCL]
        extent_time_arcsec = [0, data.shape[1]*STEPT_AV, 0, data.shape[0]*SPYSCL]
        extent_time_px     = [0, data.shape[1]*STEPT_AV, 0, data.shape[0]]

        min_extent = np.zeros(4)
        list_extent = [extent_arcsec_arcsec, extent_px_px,
                       extent_px_arcsec,extent_time_arcsec, extent_time_px]
        list_extent_coords = [('X [arcsec]','Y [arcsec]'), ('X [px]','Y [px]'),
                              ('X [px]','Y [arcsec]'),('time [s]','Y [arcsec]'),
                              ('time [s]','Y [px]')]
       

        min_extent = list(map(lambda a: abs(a[3] - a[1]), list_extent))
        min_extent = np.array(min_extent)

        if extent_arcsec_arcsec[1] == 0: min_extent[0] = 1e9

        extent_opt = list_extent[np.argmin(min_extent)]
        extent_opt_coords = list_extent_coords[np.argmin(min_extent)]

        if nover == False:
           extent = extent_arcsec_arcsec 
           ima_ref = data[:,:,50].clip(min=0, max=np.mean(data[:,:,50]))
           plt.figure()
           plt.subplot(2,1,1)
           if SPXSCL == 0.: 
              extent = extent_px_px 
              if showY == True: extent =  extent_px_arcsec
              if showtime == True:  
                 extent = extent_time_px
                 if showY == True: extent = extent_time_arcsec
           count = 1       
           if verbose == True:
               print('Extent coordinates options (ext_opt):')
               for i in list_extent_coords: 
                   print(count, i)
                   count+=1
           extent = extent_opt    
           extent_labels =  extent_opt_coords 
           if ext_opt !=0: 
               extent = list_extent [ext_opt-1]
               extent_labels = list_extent_coords [ext_opt-1]
           plt.imshow(ima_ref, origin='lower', extent=extent)
           plt.xlabel(extent_labels[0])
           plt.ylabel(extent_labels[1])
           plt.subplot(2,1,2)
           plt.plot(data[0,0,:].clip(min=0, max=5000))
           plt.tight_layout()
           plt.show()

        if no_data == True: data = None
         
        out_win = {"data":data, "wl":wl, "date_in_filename": date_in_filename, 
               "iris_obs_code": iris_obs_code, "raster_info": raster_info, 
               "DATE_OBS":DATE_OBS, "DATE_END":DATE_END, "TDET":TDET, "TDESCT":TDESCT,
               "TWAVE":TWAVE, "TWMIN":TWMIN, "TWMAX":TWMAX, "SPCSCL":SPCSCL,
               "SPXSCL":SPXSCL, "SPYSCL":SPYSCL, "STEPT_AV":STEPT_AV,
               "POS_X":POS_X, "POS_Y":POS_Y,
               "extent_arcsec_arcsec":extent_arcsec_arcsec,
               "extent_px_px":extent_px_px,
               "extent_px_arcsec":extent_px_arcsec,
               "extent_time_px":extent_time_px,
               "extent_time_arcsec":extent_time_arcsec,
               "extent_opt":extent_opt,
               "extent_opt_coords":extent_opt_coords,
               "list_extent":list_extent, 
               "list_extent_coords":list_extent_coords}

        out[label_window_info] = out_win
        if verbose !=0:
           print('Output is a dictionary with the following keywords:')
           print(out[label_window_info].keys())
           print()
        
    hdulist.close()
    return out


def only_header(filename, nover=0, window_info = 'Mg II k 2796', verbose = 0):

    hdulist = fits.open(filename)
    if verbose !=0:
       print()
       print('Reading file {}... '.format(filename))
       hdulist.info()

    hdr = hdulist[0].header
     
    return hdr


def show_lines(filename, verbose = False):

    hdulist = fits.open(filename)
    print()
    print('Reading file {}... '.format(filename))
    if verbose == True: hdulist.info()
    hdr = hdulist[0].header
    nwin  = hdr['NWIN']

    print()
    print('Available data are stored in windows labeled as:')
    # print()
    list_observed_wls = []
    for i in range(nwin): 
        print(i+1, hdr['TDESC{}'.format(i+1)], '\t Index in output: {}'.format(i))
        # list_observed_wls.append([i+1, hdr['TDESC{}'.format(i+1)]])
        list_observed_wls.append([hdr['TDESC{}'.format(i+1)]])

    return list_observed_wls


